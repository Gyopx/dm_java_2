import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){

        if (liste.isEmpty()) {
            return null;
        }

        Integer min = liste.get(0);
        for (int i = 1; i < liste.size(); i++){
            if (liste.get(i) < min)
                min = liste.get(i);
        }
        return min;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){

        for (T elem : liste){
            if (elem.compareTo(valeur) < 1)
                return false;
        }
        return true;
    }




    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        int i = 0;
        int j = 0;
        T elem = null;
        ArrayList<T> listeRes = new ArrayList<>();
        while (i < liste1.size() && j < liste2.size()){
            if (liste1.get(i).compareTo(liste2.get(j)) > 0) // si j est plus petit que i, il faut incrémenter j
                j++;
            else if (liste1.get(i).compareTo(liste2.get(j)) < 0) // si i est plus petit que j, il faut incrémenter i
                i++;
            else {
                if (liste1.get(i) != elem){ //si i et j sont égaux et que la valeur n'est pas déjà dans la liste de retour on l'ajoute et on incrémente i et j
                    listeRes.add(liste1.get(i));
                    elem = liste1.get(i);
                }
                i++;
                j++;
            }
        }
        return listeRes;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){

        String mot [] = texte.split(" ");
        List<String> listeTxt = new ArrayList<>();

        for (int i = 0; i < mot.length; i++){
            if (!mot[i].equals("")) {listeTxt.add(mot[i]);}
        }
        return listeTxt;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){

        Map<String, Integer> dicoMots = new HashMap<>();
        List<String> listeTxt = decoupe(texte);

        for ( String mot : listeTxt){
            if (dicoMots.containsKey(mot)) {dicoMots.put(mot, dicoMots.get(mot) + 1);} //si le mot est déjà présent dans le dictionnaire on incrémente sa valeur
            else {dicoMots.put(mot, 1);} // si il n'est pas présent on l'voute et on met sa valeur a 1
        }

        if (dicoMots.size()==0) {return null;}

        String valeurMax = listeTxt.get(0);
        Collections.sort(listeTxt);

        for (String mot : listeTxt){
            if (dicoMots.get(valeurMax) < dicoMots.get(mot))
                valeurMax = mot;

            if (dicoMots.get(mot).equals(dicoMots.get(valeurMax))){ // si au moins 2 mots ont le même nombre d'occurences on les ajoute à une liste triée et on récupère le premier
                List<String> listeStr = new ArrayList<>();
                listeStr.add(mot);
                listeStr.add(valeurMax);
                valeurMax = Collections.min(listeStr);
            }
        }
        return valeurMax;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        if (chaine.isEmpty())
            return true;

        int nbParentheses = 0;

        for (char c : chaine.toCharArray()){
            if (c == '(')
                nbParentheses++;

            if (c == ')')
                nbParentheses--;

            if (nbParentheses < 0)
                return false;
        }
        return nbParentheses == 0;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){

        List<Character> caracSpe = new ArrayList<>();

        for (int i = 0; i < chaine.length(); i++) {
            if (chaine.charAt(i) == '(' ||chaine.charAt(i) == '[') {
                caracSpe.add(chaine.charAt(i));
            }
            if (chaine.charAt(i) == ')' ||  chaine.charAt(i) == ']' ){
                if (caracSpe.size() == 0)
                    return false;
                else if ((  chaine.charAt(i) == ')' && caracSpe.get(caracSpe.size()-1) == '(' ) || (chaine.charAt(i) == ']' && caracSpe.get(caracSpe.size()-1) == '['))
                        caracSpe.remove(caracSpe.size()-1);
                    else
                        return false;
            }
        }
        return caracSpe.size() == 0;
    }





    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){

        if (liste.size() == 0 )
            return false;

        int milieu;
        int debut = 0;
        int fin = liste.size()-1;

        while(debut < fin){
            milieu = ((fin + debut) / 2);
            if (valeur.compareTo(liste.get(milieu)) == 0){ // si c'est égal on retourne true
                return true;
            }

            else if(valeur.compareTo(liste.get(milieu)) > 0) {debut = milieu + 1;}// si la valeur est suppérieure à la valeur du milieu on augmente la valeur du milieu de 1

            else {fin = milieu;}
        }
        return liste.get(fin).equals(valeur);
    }
}
